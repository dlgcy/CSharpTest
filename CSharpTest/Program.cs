﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClassTest
{
    /*
     * 原始：https://blog.csdn.net/tc_1337/article/details/80957140#commentsedit
     */

    class A
    {
        public static string StaticStr = "A 的静态字段";
        public string NonStaticStr = "A 的非静态字段";

        public string NoStaticProperty { get; set; } = "A 的非静态属性";

        public static void StaticMethod()
        {
            Console.WriteLine("A 的静态方法");
        }

        public void NonStaticMethod()
        {
            Console.WriteLine($"A 的非静态方法输出(非静态字段 | 非静态属性)：（{NonStaticStr} | {NoStaticProperty}）");
        }

    }

    class B : A
    {
        public static string StaticStr = "B 的静态字段";
        public string NonStaticStr = "B 的非静态字段";

        public static void StaticMethod()
        {
            Console.WriteLine("B 的静态方法");
        }

        public void NonStaticMethod()
        {
            Console.WriteLine($"B 的非静态方法输出(非静态字段)：({NonStaticStr})");
        }

    }

    class C : A
    {

    }

    class D : A
    {
        public string NonStaticStr = "D 的非静态字段";
    }

    class E : A
    {
        public string NoStaticProperty { get; set; } = "E 的非静态属性";
    }

    class F : A
    {
        public new string NoStaticProperty { get; set; } = "F 的非静态属性";
    }

    class AA
    {
        public virtual string NoStaticProperty { get; set; } = "AA 的非静态虚属性";

        public void NonStaticMethod()
        {
            Console.WriteLine($"AA 的非静态方法输出（非静态属性）：{NoStaticProperty}");
        }

    }

    class BB : AA
    {
        public override string NoStaticProperty { get; set; } = "BB 的非静态属性";
    }

    class CC : AA
    {
        public new string NoStaticProperty { get; set; } = "CC 的非静态属性";
    }

    class Program
    {
        static void Main(string[] args)
        {
            C c = new C();
            Console.WriteLine("使用 单纯继承A的C类对象 进行调用：");
            Console.WriteLine(c.NonStaticStr);      // A 的非静态字段
            c.NonStaticMethod();                    // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            A c1 = new C();
            Console.WriteLine("使用 以C类实例化的A类对象 进行调用：");
            Console.WriteLine(c1.NonStaticStr);     // A 的非静态字段
            c1.NonStaticMethod();                   // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            B b = new B();
            Console.WriteLine("使用 覆盖了A类成员的B类对象 进行调用：");
            Console.WriteLine(b.NonStaticStr);      // B 的非静态字段
            b.NonStaticMethod();                    // B 的非静态方法输出(非静态字段)：(B 的非静态字段)
            Console.WriteLine("-------------------------------\r\n");

            A b1 = new B();
            Console.WriteLine("使用 以B类实例化的A类对象 进行调用：");
            Console.WriteLine(b1.NonStaticStr);     // A 的非静态字段
            b1.NonStaticMethod();                   // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            D d = new D();
            Console.WriteLine("使用 仅覆盖了A类非静态字段的D类对象 进行调用：");
            Console.WriteLine(d.NonStaticStr);     // D 的非静态字段
            d.NonStaticMethod();                   // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            E e = new E();
            Console.WriteLine("使用 仅覆盖了A类非静态属性的E类对象 进行调用：");
            Console.WriteLine(e.NoStaticProperty); // E 的非静态属性
            e.NonStaticMethod();                   // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            F f = new F();
            Console.WriteLine("使用 使用new关键字覆盖了A类非静态属性的F类对象 进行调用：");
            Console.WriteLine(f.NoStaticProperty); // F 的非静态属性
            f.NonStaticMethod();                   // A 的非静态方法输出(非静态字段 | 非静态属性)：（A 的非静态字段 | A 的非静态属性）
            Console.WriteLine("-------------------------------\r\n");

            BB bb = new BB();
            Console.WriteLine("使用 仅覆盖了AA类非静态虚属性的BB类对象 进行调用：");
            Console.WriteLine(bb.NoStaticProperty); // BB 的非静态属性
            bb.NonStaticMethod();                   // AA 的非静态方法输出（非静态属性）：BB 的非静态属性
            Console.WriteLine("-------------------------------\r\n");

            CC cc = new CC();
            Console.WriteLine("使用 new了AA类非静态虚属性的CC类对象 进行调用：");
            Console.WriteLine(cc.NoStaticProperty); // CC 的非静态属性
            cc.NonStaticMethod();                   // AA 的非静态方法输出（非静态属性）：AA 的非静态虚属性
            Console.WriteLine("-------------------------------\r\n");

            AA ab = new BB();
            Console.WriteLine("使用 以BB类实例化的AA类对象 进行调用：");
            Console.WriteLine(ab.NoStaticProperty); // BB 的非静态属性
            ab.NonStaticMethod();                   // AA 的非静态方法输出（非静态属性）：BB 的非静态属性
            Console.WriteLine("-------------------------------\r\n");

            Console.ReadKey();
        }
    }
}
